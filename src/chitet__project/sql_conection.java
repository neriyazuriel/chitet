/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chitet__project;

import objects.Player;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import static sun.audio.AudioPlayer.player;
import objects.Player;

/**
 *
 * @author User
 */
public class sql_conection {
    public Connection conn;
    public PreparedStatement ps;
    public ResultSet rs;
    
    // יצירת חיבור למסד
    private Connection createConnection(){
        
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            String url = "jdbc:sqlserver://localhost:1433;databaseName=chitet_project;integratedSecurity=true;";
            conn = DriverManager.getConnection(url);
        }catch (ClassNotFoundException | SQLException ex){
            conn = null;
            System.out.println("Error: C.CreateSQL" + ex);
        }
    return conn;
    }
    
    // סגירת החיבור ומשתני העזר שלו
    /*private void closeConnection (){ 
        try {
            //if (!rs.isClosed())
                rs.close();
            //if (!ps.isClosed())
                ps.close();
            //if (!conn.isClosed())
                conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(sql_conection.class.getName()).log(Level.SEVERE, null, ex);
        }
    }*/
    
    // 
    public int ChitetLogin(Player player){
        int result = -1;
        try {
        Connection conn = createConnection();
        String sql = "exec p_check_login ?,?";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(1, player.getName());
        ps.setString(2,player.getPassword());
        ResultSet rs = ps.executeQuery();
        while (rs.next()) result = rs.getInt(1);
        //closeConnection();
    } catch (SQLException ex){
        System.out.println("ERROR: CheckLogin" + ex);
    }
        return result;
    
    }
    public int ChitetRegister(Player player){
        int result = -1;
        try {
        Connection conn = createConnection();
        String sql = "exec RegisterPlayer ?,?";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(1, player.getName());
        ps.setString(2, player.getPassword());
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            result = rs.getInt(1);
        }
        if (result == 0)
                System.out.println("Created successfully");
        else if (result == 1)
                System.out.println("User already exists");
        //closeConnection();
    } catch (SQLException ex){
        ex.printStackTrace();
    }
        return result;
    }
    /*public void shuffle(Round round){
        ArrayList<Integer> pack = new ArrayList();
        
        try{
            Stringsql = "exec p_cards_gameLastPack ?";
            ResultSet row;
            CreateConnection();
            String sql;
            
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, round.packID+"");
            
        
        }*/
    
    
}
