/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objects;

/**
 *
 * @author User
 */
public class Player {
    
    private int ID = 0;
    private String name = "";
    private String password = "";
    private String status = "";

    public Player() {
    
    }    

    public int getID() {
        return ID;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public String getStatus() {
        return status;
    }
    
    public void setID(int ID) {
        this.ID = ID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "name: " + name + "\n" +
                "password: " + password + "\n" +
                "status: " + status + "\n" +
                "ID: " + ID;
    }
    
    
}
